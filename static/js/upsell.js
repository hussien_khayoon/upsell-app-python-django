
var onCheckoutPageIdx = window.location.href.indexOf("//checkout.shopify.com/");

if (onCheckoutPageIdx!=-1) {

	function checkedProduct(elem){
		if (elem.checked){
			console.log("here");
			var id = elem.getAttribute("id");
			console.log(elem.value);
		} else{
			
		}
	}
	
	
	function do_ajax_post_without_jquery( url, body, success_callback, fail_callback)	
	{
	    var xmlhttp = new XMLHttpRequest();
		
	    xmlhttp.onreadystatechange = function() {
	        if (xmlhttp.readyState == 4 ) {  //XMLHttpRequest.DONE 
	        	console.log("xmlhttpstatus is " + xmlhttp.status);
	           if(xmlhttp.status == 200){
					success_callback(document, xmlhttp)
	           }
	           else  {
					fail_callback(document, xmlhttp)
	           }
	        } 
	    }
		// "GET", "URL", "async (true/false)"
		xmlhttp.open('POST', url, true);
		
		xmlhttp.withCredentials = true;
		xmlhttp.setRequestHeader("Content-type", "application/json");
		
		xmlhttp.send(body)
	}
	
	function findFirstMatchingNodeByAttribute(allElements, attribute, contains_text)
	{
	  for (var i = 0, n = allElements.length; i < n; i++)
	  {
	  	src_attribute = allElements[i].getAttribute('src'); 
	
	    if (src_attribute.indexOf(contains_text) >= 0)
	    {
	    	return allElements[i];
	    }
	  }
	  return null;
	}
	
	function getUrlParamForUrl(url, name) {
		  var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
		  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}
	
	
	function get_shop_name_from_script_call() {
	
		var script_node = findFirstMatchingNodeByAttribute( document.getElementsByTagName('script'), 
															'src', 
															'upsell.js');
		
		return getUrlParamForUrl(script_node.getAttribute('src'), 'shop');
	
	}
	
	function get_legacy_json_string() {
		var paramObj = {};
	
		var substr = window.location.href.substring(onCheckoutPageIdx+"//checkout.shopify.com/".length).split("/");
		var token = substr[2].split("?")[0];
	
		paramObj['shopDomain'] = get_shop_name_from_script_call();
		paramObj['token'] = token;		
		
		return JSON.stringify(paramObj);
	}

	
	function get_json_string(node) {
	
		var paramObj = {};
		
		shop = node.getAttribute('data-shop');
		
		if (shop == null) {
			shop = get_shop_name_from_script_call();
		}
		
		paramObj['shop_url'] = shop;
		paramObj['order_id'] = node.getAttribute('data-order-id');
		paramObj['customer_email'] = node.getAttribute('data-customer-email');
		paramObj['customer_name'] = node.getAttribute('data-customer-name');
		
		return JSON.stringify(paramObj);
	}
	
	function checkoutUpsell(){
		
		// see which productid is checked
		// grab their quantity and variant info
		// send it to the server to create permalink
		var jsonArr = [];
		console.log(window.upsell_count);
		
		for (i=0; i<window.upsell_count; i++){
			if (document.getElementById('check'+i).checked){
				quantity = document.getElementById('quantity'+i).value;
				product_id = document.getElementById('product_id'+i).value
				ref        = document.getElementById('ref').value
				variant = document.getElementById('variant'+i).children;

				var myObj = {};

				
				myObj["product_id"] = product_id;
				myObj["ref"] = ref;
				myObj["quantity"] = quantity;
				if (variant.length > 0){
					for (j=0; j<variant.length; j++){
					    if (variant[j].id != ''){
					    	option = variant[j].id
					    	option_value = variant[j].value;
					    	console.log(option);
					    	console.log(option_value);
							
							// this is to make the variable names more sequenced
							// 0,1,2 instead of 1,2,4
							if(j==0) t=0;
							if(j==2) t=1;
							if(j==4) t=2;
								
							myObj["variant"+t] = option;
							myObj["variant"+t+"_value"] = option_value;
					    }else{
					    	//do nothing, it's just a <br> tag
					    }

					}
				}
				//if there's no variants, then these value of variants need to be blank
				// so that the JSON being sent to the server is consistent	
				else{
					myObj["variant0"]       = '';
					myObj["variant0_value"] = '';
					myObj["variant1"]       = '';
					myObj["variant1_value"] = '';
					myObj["variant2"]       = '';
					myObj["variant2_value"] = '';
				}
			    jsonArr.push({
			    	product_id:     product_id,
			    	quantity:       quantity,
			    	ref     :       ref,
			    	variant0:       myObj["variant0"],
			    	variant0_value: myObj["variant0_value"],
			    	variant1:       myObj["variant1"],
			    	variant1_value: myObj["variant1_value"],
			    	variant2:       myObj["variant2"],
			    	variant2_value: myObj["variant2_value"]
			    });
			}
		}
		
		var json_body = JSON.stringify(jsonArr);
		console.log(json_body);
		
		do_ajax_post_without_jquery('https://upsell.elasticbeanstalk.com/chosenUpsellCheckout',
				json_body, 
				function(doc, xmlhttp) 
				{ 				
					console.log("SUCCESS");
					permalink = JSON.parse(xmlhttp.responseText).permalink;
					window.location = permalink;

				},
				function(doc, xmlhttp) { 
					console.log("FAIL");	
				
				});

	}

	var node = document.getElementById("forewards-checkout"); 

	var json_body;
	
	if (node != null)
	{
		json_body = get_json_string(node);	
	}
	else
	{
		json_body = get_legacy_json_string();
	}

	
	do_ajax_post_without_jquery('https://upsell.elasticbeanstalk.com/showUpsell', 
								json_body, 
								function(doc, xmlhttp) 
								{ 				
									console.log("SUCCESS");	
									// Create element holding the response HTML
									var element = document.createElement('div');
									element.innerHTML = JSON.parse(xmlhttp.responseText).message;
									
									window.upsell_count = JSON.parse(xmlhttp.responseText).upsell_products_count;
                                    console.log(window.upsell_count);
									// Append it to the end of the body..
									var node = doc.getElementsByTagName('body')[0]
									node.appendChild(element);  
									
									window.location.hash="jump";
									
									window.plus = document.getElementsByClassName('qtyplus');

									for (var i=0; i < window.plus.length; i++) {
										window.plus[i].onclick = function(e){
											  e.preventDefault();
											  console.log("here");
											  var field = this.getAttribute("field");
											  var elem = document.getElementById(field);
											  var currentVal = parseInt(elem.value);
											  if(!isNaN(currentVal)){
												  elem.value = currentVal+1;
											  } else {
												  elem.value = 0;
											  }
										  }
									}

									window.minus = document.getElementsByClassName('qtyminus');
									for (var i=0; i < window.minus.length; i++) {
										window.minus[i].onclick = function(e){
											  e.preventDefault();
											  var field = this.getAttribute("field");
											  var elem = document.getElementById(field);
											  var currentVal = parseInt(elem.value);
											  if(!isNaN(currentVal) && currentVal > 0 ){
												  elem.value = currentVal-1;
											  } else {
												  elem.value = 0;
											  }
										  }
									}
									
									/*
									window.checked_upsell = document.getElementsByClassName('checked');
									for (var i=0; i< window.checked_upsell.length; i++){
										window.checked_upsell[i].onclick = function(e){
											e.preventDefault();
											if (window.checked_upsell[i].checked){
												var id = this.getAttribute("id");
												var elem = document.getElementById(id);
												console.log("checked");
												console.log(elem.value);
											}
										}
									}
									*/
									

								},
								function(doc, xmlhttp) { 
									console.log("FAIL");	
								
								});


}