def getBooleanFromValue(theString):
    '''
    Deal with improperly padded config files or env entries.. Python does not deal with boolean strings
    very well.
    '''
    result = {'true': True,  '1':True, 'on': True, 'yes': True}.get(theString.lower())

    if result is None:
        result = False

    return result