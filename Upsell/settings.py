"""
Django settings for Upsell project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from utility import getBooleanFromValue
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'xt4^xrkk2+wjh1i&m)cwv8#-v)(__gme97i^8bg4__uyfg(v@h'

# SECURITY WARNING: don't run with debug turned on in production!

DEPLOYED       = getBooleanFromValue(os.environ.get("DEPLOYED", "False"))
USE_SSL        = getBooleanFromValue(os.environ.get('USE_SSL', 'True' if DEPLOYED  else 'False'))
DEBUG          = getBooleanFromValue(os.environ.get('DEBUG',   'False' if DEPLOYED  else 'True'))
TEMPLATE_DEBUG = getBooleanFromValue(os.environ.get('TEMPLATE_DEBUG',   'False' if DEPLOYED  else 'True'))

# TODO: THIS IS TEMPORARY DO NOT FORGET IN PRODUCTION
ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'storages',
    'shop',
    'sslserver',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'Upsell.urls'

WSGI_APPLICATION = 'Upsell.wsgi.application'

#USE_SSL = True


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

''' 
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
'''

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('UPSELL_DB_NAME', 'upsell'),
        'USER': os.environ.get('UPSELL_DB_USERNAME', 'root'),
        'PASSWORD': os.environ.get('UPSELL_DB_PASSWORD', '@miR2$12'),
        'HOST': os.environ.get('UPSELL_DB_DOMAIN', 'localhost'),   # Or an IP Address that your DB is hosted on
        'PORT': os.environ.get('UPSELL_DB_PORT', '3306')
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME", '')
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", '')
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", '')

# Tell django-storages that when coming up with the URL for an item in S3 storage, keep
# it simple - just use this domain plus the path. (If this isn't set, things get complicated).
# This controls how the `static` template tag from `staticfiles` gets expanded, if you're using it.
# We also use it in the next setting.
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

# This is used by the `static` template tag from `static`, if you're using that. Or if anything else
# refers directly to STATIC_URL. So it's safest to always set it.
#STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN


# Tell the staticfiles app to use S3Boto storage when writing the collected static files (when
# you run `collectstatic`).
if DEPLOYED:
    STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
else:
    STATIC_URL = '/static/'




TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

SHOPIFY_APP_KEY = os.environ.get('FOREWARDS_SHOPIFY_APP_KEY', 'bd7636a9f11d2f0e9c275a172ef6b9fb')
SHOPIFY_SHARED_SECRET = os.environ.get('FOREWARDS_SHOPIFY_SHARED_SECRET', 'c7f0ad9defe67a8fdf0f4bec99bd6657')
SHOPIFY_API_SCOPE = ['read_products', 'read_customers', 'read_orders', 'write_script_tags', 'write_themes']



ADMINS = (
    ('Hussien', 'gorelami@gmail.com')
)

SERVER_EMAIL = os.environ.get('SERVER_EMAIL', 'django-local@toocoomedia.com')


UPSELL_LOG_ROOT = os.environ.get("UPSELL_LOG_ROOT", "logs")


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(thread)d %(funcName)s() line: %(lineno)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'DEBUG',
            'class':'logging.FileHandler',
            'filename': os.path.join(UPSELL_LOG_ROOT,'upsell.log'),
            'formatter':'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['file', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'upsell': {
            #'handlers': ['console', 'file'],
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
        },
    }
}

