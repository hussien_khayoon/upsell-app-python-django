from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Upsell.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^install$', 'shop.views.install'),
    url(r'^shop/finalize/?$',  'shop.views.finalize'),
    url(r'^$', 'shop.views.login'),
    url(r'^dashboard$', 'shop.views.dashboard'),
    url(r'^analytics$', 'shop.views.stats'),
    url(r'^random', 'shop.views.randomUpsellProduct'),
    url(r'^showUpsell$', 'shop.views.showUpsell'),
    url(r'^randomUpsell$', 'shop.views.randomUpsellProduct'),
    url(r'^saveSelectedUpsell$', 'shop.views.saveSelectedUpsell'),
    url(r'^removeSelectedUpsell$', 'shop.views.removeSelectedUpsell'),
    url(r'^chosenUpsellCheckout$', 'shop.views.chosenUpsellCheckout'),
    url(r'^webhooks/uninstall',    'shop.views.app_uninstall'),
    
)
