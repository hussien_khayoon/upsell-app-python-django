from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.conf import settings as django_settings
import shopify
import logging
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseNotFound, HttpResponse
from shop.models import Shop, Upsell, UpsellStats, ReferralRecord
import uuid
import json
import datetime
import os
import traceback
import random
import jsonpickle
import simplejson
import base64
from django.core import serializers
logger = logging.getLogger('upsell')

# Create your views here.
# This is the entry point for manually installing the application on your shopify store.
@csrf_exempt
def install(request):
    # The user enters their shopify store name, we get it from the 'shop' variable in the header
    if request.method == 'POST':
        shopify_domain_name = request.REQUEST.get('shop')
        
        # if the shop name is entered the OAuth2 process beings.
        # we build our permission_url using our shopify shared, secret keys and our shop name.
        # we then redirect to that permission_url, which leads us to the login() method
        if shopify_domain_name:
            shopify.Session.setup(api_key=django_settings.SHOPIFY_APP_KEY, secret=django_settings.SHOPIFY_SHARED_SECRET)
            session         = shopify.Session(shopify_domain_name.strip())            
            redirect_uri    = request.build_absolute_uri(reverse('shop.views.finalize'))
            logger.info(redirect_uri)
            permission_url  = session.create_permission_url(django_settings.SHOPIFY_API_SCOPE, redirect_uri)

            logger.debug(permission_url)
            return redirect(permission_url)
    
        return HttpResponseNotFound("No shop name found.")
    
        
    return render_to_response('shop/install.phtml',
    context_instance=RequestContext(request))


# shopify redirect customer here when user agrees to install (redirect from permission_url)
def finalize(request):
    logger.debug("finalize() called")
    # collect shop, code, timestamp, signature
    params = {}
    params['shop']      = request.REQUEST.get('shop','')
    params['code']      = request.REQUEST.get('code','')
    params['timestamp'] = request.REQUEST.get('timestamp','')
    params['signature'] = request.REQUEST.get('signature','')
    params['hmac']      = request.REQUEST.get('hmac','')
    
    
    if params['code'] == '':
        logger.error("no temp token found")
        return HttpResponseNotFound("No temp token found, cannot process")
    
    # we create a new Shopify session 
    session = shopify.Session(params['shop'])
    
    # we use that session and the url params to request our permanent access token
    try:
        token = session.request_token(params)
        
        # setup our shopify session for an API call
        session = shopify.Session(params['shop'], token)
        shopify.ShopifyResource.activate_session(session)
    except Exception:
        logger.error("Could not get permanent access token", exc_info = True)
        return HttpResponseNotFound("Could not get permanent access token")
        
    # we check to see if the shop is already installed
    shop = Shop.objects.filter(domain = params['shop'])
    
    # if the shop is already installed we start a django session with our shop
    # this will help us grab the right access token for the shop from the db
    if shop:
        request.session['shop'] = params['shop']
        shop[0].installed = True
        shop[0].access_token = token
        shop[0].save()
    else:
        # Here, we do our first shopify API call to get our shop name
        shopify_shop    = shopify.Shop.current()
        shop_currency   = shopify_shop.currency
        if not shopify_shop:
            logger.error("shopify api call failed to return the shop")

        # we fill our db with our new shop info
        new_shop = Shop()
        new_shop.domain = params['shop']
        new_shop.name = shopify_shop.name
        new_shop.shop_email = shopify_shop.email
        new_shop.access_token = token
        new_shop.is_installed = True
        new_shop.save()
        
        
    request.session['shop'] = params['shop']
    
    # looks for old script tags and delete them
    scripttags = shopify.ScriptTag.find()
    for scripttag in scripttags:
        if "upsell" in scripttag.src:
            logger.debug( 'deleting scripttag ' + scripttag.src)
            scripttag.destroy()
    
    # install new script tag
    logger.info('installing script tag for shop: ' + str(params['shop']))
    new_scripttag       = shopify.ScriptTag()
    new_scripttag.src   = "https://s3.amazonaws.com/wso20120423/upsell.js"
    new_scripttag.event = "onload"
    
    if not new_scripttag.save():
        logger.debug( new_scripttag.errors.full_messages())

    # register uninstall webhook
    webhooks = shopify.Webhook.find(topic="app/uninstalled")
    # delete old webhook
    for webhook in webhooks:
        logger.debug('deleting webhook '+ webhook.topic + ' ' + webhook.address)
        webhook.destroy()
    
    # install new webhook
    new_webhook         = shopify.Webhook()
    new_webhook.topic   = "app/uninstalled"
    new_webhook.address = "https://" + os.environ.get('UPSELL_DOMAIN_NAME', '') + "/webhooks/uninstall"
    new_webhook.format  = "json"
    if not new_webhook.save():
        logger.debug( new_webhook.errors.full_messages())
    
    return redirect(reverse('shop.views.dashboard'))


@csrf_exempt
def app_uninstall(request):
    #when the webhook is triggered this method should be called the /webhook/uninstall endpoint
    json_data = json.loads(request.body)
    response = ''

    # find shop url from json data
    shop_url = json_data.get('myshopify_domain')
    if not shop_url:
        logger.error('myshopify_domain is not returned by webhook app uninstalled')

    # find shop by shop_url
    shop = Shop.objects.filter(domain=shop_url).first()
    if not shop:
        logger.error('trying to uninstall a shop that we do not recognize')
        response =  HttpResponseNotFound("Uninstall DID NOT WORK")
        return response
     
    logger.info("Uninstall for shop: " + shop.domain)
    
    shop.is_installed = False
    shop.save()
    
    logger.info(str(shop_url) + ' uninstalled successfully')
    response = HttpResponse()
    return response
    
# Shopify app entry point, i.e. customer click on the app icon from their shopify admin/apps page
def login(request):
    logger.debug("login() called")
    
    #TODO: check for cookie here 
    
    shop_url        = request.REQUEST.get('shop', '')
    
    if not shop_url:
        return HttpResponseNotFound("No shop found")

    params = {}
    params['shop']      = request.REQUEST.get('shop','')
    params['timestamp'] = request.REQUEST.get('timestamp','')
    params['signature'] = request.REQUEST.get('signature','')
    params['hmac']      = request.REQUEST.get('hmac','')
    
    if  params['signature'] == '':
        return HttpResponseNotFound("Could not verify user")
    
    # we create a new Shopify session 
    shopify.Session.setup(secret=django_settings.SHOPIFY_SHARED_SECRET)
    shopify_session = shopify.Session(shop_url)
    verified        = shopify_session.validate_params(params)
    
    if not verified:
        logger.debug('not verified')
        return render_to_response('shop/need_login.phtml')  # signature not correct, redirect to a page show error
    
    elif Shop.objects.filter(domain=shop_url, is_installed=True).count() == 0:
        logger.debug('shop not in db')
        return redirect(reverse('shop.views.install')) #shop not in db, redirect to install page
    else:
        shop                = Shop.objects.filter(domain=shop_url)[0]
        shop.login_cookie   = uuid.uuid4()   
        shop.save()
        request.session['shop'] = shop_url
  
    return redirect(reverse('shop.views.dashboard'))

@csrf_exempt
def dashboard(request):
    shop_name = request.session['shop']
    
    if shop_name == '':
        logger.error("No shop session found here")
        return HttpResponseNotFound("No shop session found")
    
    shop    = Shop.objects.filter(domain = shop_name).first()
    
    if not shop:
        logger.error("Error: No shop found in the database for one in the session")
        
    
    session  = shopify.Session(shop_name, shop.access_token)
    shopify.ShopifyResource.activate_session(session)
    
    products                    = shopify.Product.find(limit=250)
    
    if request.method == 'POST':
        logger.debug("sending post data via /dashboard")
        try:
            json_data   = json.loads(request.body)
            product_id  = int(json_data.get('product_id'))
            

            #check db first to see list of upsells
            upsell                              = Upsell.objects.filter(main_product = product_id).first()
            existing_upsell_products            = []
            main_product                        = []
            serialized_existing_upsell_products = []
            index                               = ''
            
            # after getting the index of our main_product we add it to our new list
            if product_id in products:
                index = products.index(product_id)

            main_product.append(products[index])
            serialized_main_product  = jsonpickle.encode(main_product) 
            
            if not upsell:
                logger.info("No upsells found for this product")
            else:
                existing_upsell_products = upsell.upsell_products
                #existing_upsell_products.remove(product_id)
                
                temp_products = []

                # look through all products in the store
                # if product is found in our existing upsell product list. Add that product object
                # to our temporary product list                
                for product in products:
                    if product.id in existing_upsell_products:
                        temp_products.append(product)
                        
                # must be serialized here to avoid the appending of the existing products we do next
                serialized_existing_upsell_products = jsonpickle.encode(temp_products)

            
            # we are appending this product_id in order to not show in the list of Available upsell products
            existing_upsell_products.append(product_id)
 
            # This is to get all available upsell products.
            # we remove all product objects that exist in our existing upsell product list.
            # including the product itself(see previous statement)

            for product in existing_upsell_products:
                if product in products:
                    products.remove(product)

            available_upsell_products = products   
            
            # we return an array of JSON products object to be manipulated by AngularJS in the frontend
            serialized_available_upsell_products  = jsonpickle.encode(available_upsell_products)                        
            
            all_upsell_products = {}
            all_upsell_products['main']      = serialized_main_product
            all_upsell_products['available'] = serialized_available_upsell_products
            all_upsell_products['existing']  = serialized_existing_upsell_products

            return HttpResponse(json.dumps(all_upsell_products), content_type='application/json')
        
        except Exception as e:
            logger.error(e)
            return HttpResponse()

        
    return render_to_response('shop/dashboard.phtml', {'products': products, 'shop': shop},
    context_instance=RequestContext(request))


@csrf_exempt
def stats(request):
    # lets query the upsell stats table and display it for the shop
    shop_name    = request.session['shop']
    shop         = Shop.objects.filter(domain = shop_name)
    upsell_stats = UpsellStats.objects.filter(shop = shop)
    
    if not upsell_stats:
        logger.error("Ther is not upsell_stats table in the stats view")
        
    
    # this is an experiment to see how easy it is to serialize Django objects to JSON
    # in order to see if AngularJS can take the data and manipulate it.
    # The issue faced was serializing a Queryset from the Django model and I had to use
    # the django.core serializer instead of the python json library
    # this proved to me that I should have just used django REST API framework instead
    upsell_stats_json = serializers.serialize('json', upsell_stats)

    
    return render_to_response('shop/stats.phtml', {'upsell_stats_json': upsell_stats_json},
    context_instance=RequestContext(request))

@csrf_exempt
def showUpsell(request):
    logger.debug('order_confirmation_event() called with method ' + request.method)


    if request.method == 'OPTIONS':
        return _init_callback_response_header(request)

    # get post data
    json_data      = json.loads(request.body, parse_int=int)
    shop_domain    = json_data.get('shop_url')
    order_id       = json_data.get('order_id')
    product_id     = json_data.get('product_id')
    
    
    if order_id:
        shop        = Shop.objects.filter(domain=shop_domain).first()

    legacy_mode = False


    # If we don't see a shop_domain at the new param location, check the older 'legacy' parameters
    if not shop_domain:
        # get post data
        legacy_mode = True

        json_data   = json.loads(request.body)
        shop_domain = json_data.get('shopDomain')
        check_token = json_data.get('token')
        

        
        # this is for in case the user refershe the screen, we remove #jump from the token
        check_token = check_token.replace("#jump", "")
        request.session['shop_domain'] = shop_domain
        logger.info("shop domain is " + str(shop_domain))
        logger.info("shop domain in the session is: " + str(request.session['shop_domain']))

        if not shop_domain or not check_token:
            logger.error('shopDomain or token not found in POST data')
            return _init_callback_response_header(request)

        shop        = Shop.objects.filter(domain=shop_domain).first()
        
        # find out which the product that got sold through the order
        #TODO: note the case where they buy multiple products
        # for now let's just choose the first one
        
        session  = shopify.Session(shop.domain, shop.access_token)
        shopify.ShopifyResource.activate_session(session)
        
        order = shopify.Order.find(checkout_token = check_token, status = any)
        
        if not order:
            logger.error("No order found for this checkout token, don't know what upsell prodcut to choose")
        
        # let's find the product in this order (just the first for now)
        ordered_product_id = order[0].line_items[0].product_id
        
        upsell = Upsell.objects.filter(main_product = ordered_product_id).first()
        
        # if there are no upsell products attached to this product, return no message
        if not upsell:
            logger.info("No upsells found for this product")
            return HttpResponse()
        
        response = HttpResponse("", content_type='application/json')
        
        # doing an api call for now to get the product, be we should just store it in the db
        # we need the image link , title and price stored for each upsell and it should be good
        upsell_products = []
        for upsell_product in upsell.upsell_products:
            product = shopify.Product.find(int(upsell_product))
            upsell_products.append(product)
            
        logger.debug("the upsell products is/are" + str(upsell_products))
        
        
        # update the view stats
        upsell_stats = UpsellStats.objects.filter(shop = shop).first()
        
        if not upsell_stats:
            logger.error("There is no upsell_stats result no showUpsell")
        
        # see if the sale came from our app by checking the cookie or the ref in the order
        ref = order[0].landing_site_ref
        refRecord = ReferralRecord.objects.filter(ref = ref)

        
        
        if refRecord:
            #TODO: when the page refreshes, make sure the sales stats aren't incremented again
            logger.info("order id for this is: " + str(order[0].id))
            logger.info("The ref from the order is: " + str(ref))
            # increase sales stat by 1
            upsell_stats.sales += 1
            upsell_stats.sales_amount += float(order[0].total_price)
            upsell_stats.save()

        
        # if it is from us delete the cookie
        
        # since a sale happened let's create a ref record for it
        customer = order[0].customer
        referralRecord                      = ReferralRecord()
        referralRecord.shop                 = shop
        referralRecord.ref                  = generate_ref()
        referralRecord.order_id             = order[0].id
        referralRecord.customer_first_name  = customer.first_name if customer.first_name else ''
        referralRecord.customer_last_name   = customer.last_name if customer.last_name else ''
        referralRecord.customer_email       = customer.email if customer.email else ''
        referralRecord.save()
        
        
        
        # initialized upsell message string
        upsell_message = ''
        upsell_message_style = ''
        message        = ''
        
        i = 0
        for upsell_product in upsell_products:
                
            upsell_message_style = '''
                            <link rel="stylesheet" href="https://s3.amazonaws.com/wso20120423/style.css">
                            <style>
                            .msg-content {
                                display: inline-block;
                                font-family: 'Pontano Sans','sans-serif';
                                text-align: left;
                                line-height:1.5em;
                                width: 50%;
                                border-radius: 25px;
                                border: 2px solid #000000;
                                padding: 20px;
                                margin-bottom: 20px;
                            }
                            
                            .qty {
                                width: 40px;
                                height: 40px;
                                text-align: center;
                                border: 2px solid #dadada;
                            }
                            input.qtyplus { width:40px; height:40px;}
                            input.qtyminus { width:40px; height:40px;}
                            
                            /* stop highlighting border on click for + and - */
                            input.qtyplus:focus{
                                outline-width: 0;                           
                            }
                            input.qtyminus:focus{
                                outline-width: 0;
                            }                            

                            
                            </style>
                            

                    '''
            

            # if there are no varians on the prodcut, than this variable is empty
            upsell_message_variant_names_final = '' 
            
            # we find out if a product has variants by seeing if it has more than one variant.
            # this is because products that have no variants on Shopify have one variant only(in the JSON)             
            if len(upsell_product.variants) > 1:
                upsell_message_variant_names = "{0} <select id='{0}'> {1} </select> <br />"
                # go through all variants
                for option in upsell_product.options:
                    # go through all options in variant
                    # wipe out the previous option values
                    upsell_message_variants_values_all =''
                    for value in option.values:
                        upsell_message_variants_values = "<option value='{0}'> {0}  </option>"
                        upsell_message_variants_values_all += upsell_message_variants_values.format(value)
                    upsell_message_variant_names_final += upsell_message_variant_names.format(option.name, upsell_message_variants_values_all)
    

            upsell_message_content = ''' 
                                    <div>
                                        <div class="msg-content">
                                            <div style="float: left; width: 20%%">
                                                <div>
                                                    <ul>
                                                        <li>
                                                            <input type="checkbox" id="check%s" class='checked' onclick="checkedProduct(this)" field="%s" checked/>
                                                            <label for="check%s"></label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                
                                                <div class="qty_main_div" style="margin-top: 40%%; padding-left: 25%%">
                                                <form id='myform' method='POST' action='#' style="font-family:FontAwesome;font-size:2em;">
                                                    <input type='button' value='&#xf196' class='qtyplus' field='quantity%s' /><br />                                               
                                                    <input type='text' id='quantity%s' value='1' class='qty' /><br />
                                                    <input type='button' value='&#xf147;' class='qtyminus' field='quantity%s' />
                                                </form>
                                                </div>
                                            </div>
                                            <input type="hidden" id="product_id%s" value="%s" />
                                            <input type="hidden" id="ref" value="%s" />
                                            
                                            <div class="upsell_image" style="display: inline;">
                                                <img src="%s" height="200" width="200">
                                            </div>
                                        
                                            <div class="products_price_variants" style="float: right; width: 40%%">
                                                <div>
                                                    <h1>%s</h1>
                                                    <h1 style="margin-top: 5%%">$%s</h1>
                                                </div>
                                                <div id='variant%s'>
                                                    %s
                                                </div>
                                            </div>
                                            <a name="jump"></a>
                         
                                        </div>
                                    </div>
                        '''
                    
            upsell_message = upsell_message_content
                            
            message += upsell_message % (i, upsell_product.id, i, i, i, i, i, upsell_product.id, referralRecord.ref, upsell_product.image.src, upsell_product.title, upsell_product.variants[0].price, i, upsell_message_variant_names_final)
            i += 1
        
    
        #combine style with upsell_message
        message = '<div style="text-align:center"><h1 style="margin-bottom: 10px;"> Other Products You Might Like</h1>' + message + upsell_message_style +'<div><img src="https://simplecreditcardpayments.com/wp-content/themes/headlines/authnet_images/authnet-buy-now.png" onclick="checkoutUpsell()" height="200" width="400"></div></div>'
        response.write(json.dumps({'message': message, 'upsell_products_count': i}))
        
        origin = request.META.get('HTTP_ORIGIN')
        response['Access-Control-Allow-Origin'] = origin
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Credentials'] = 'true'
        response['Access-Control-Allow-Headers'] = request.META.get('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', "X-Requested-With, Content-Type, Content-Length, Origin, Accept, Accept-Encoding, Referer, User-Agent, Host, Cookie")
        

        if not upsell_stats:
            logger.error("there are no upsell_stat rows, something is wrong")
        else:
            upsell_stats.views += 1
            upsell_stats.save()
        
        return _init_callback_response_header(request, response)

def _init_callback_response_header(request, response = None, response_code = None, response_message = None):

    temp_response = response
    if not temp_response:
        logger.debug("we're in here now")
        temp_response = HttpResponse("", content_type='application/json')

    origin = request.META.get('HTTP_ORIGIN')
    temp_response['Access-Control-Allow-Origin'] = origin
    temp_response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
    temp_response['Access-Control-Allow-Credentials'] = 'true'
    temp_response['Access-Control-Allow-Headers'] = request.META.get('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', "X-Requested-With, Content-Type, Content-Length, Origin, Accept, Accept-Encoding, Referer, User-Agent, Host, Cookie")

    # for debugging
    if request.method == 'OPTIONS':
        logger.debug('allow origin ' + str(origin))

    return temp_response

def randomUpsellProduct(request):
    shop_name = request.session['shop']
    
    if shop_name == '':
        logger.error("No shop session found here")
        return HttpResponseNotFound("No shop session found")
    
    shop    = Shop.objects.filter(domain = shop_name).first()
    
    if not shop:
        logger.error("Error: No shop found in the database for one in the session")
        
    
    session  = shopify.Session(shop_name, shop.access_token)
    shopify.ShopifyResource.activate_session(session)
    
    products  = shopify.Product.find(limit=250)
    
    x = random.randint(0,len(products)-1)
    
    product = products[x]

    return render_to_response('shop/dashboard.phtml',{'product': product, 'shop': shop},
    context_instance=RequestContext(request))
    
    
@csrf_exempt
def saveSelectedUpsell(request):
    shop_name = request.session['shop']
    if request.method == 'POST':
        logger.debug("sending post data via /saveSelectedUpsell")
        try:
            json_data   = json.loads(request.body)
            product_id  = json_data.get('product_id') 
            selection   = json_data.get('selection')
            
            # store the product and its upsells in the db
            
            upsell = Upsell.objects.filter(main_product = product_id).first()
            shop   = Shop.objects.filter(domain = shop_name).first()
            
            if not upsell:
                upsell                  = Upsell()
                upsell.shop             = shop
                upsell.main_product     = product_id
                upsell.upsell_products  = selection
                upsell.save()
            else:
                upsell.main_product     = product_id
                if upsell.upsell_products and upsell.upsell_products !='':
                    upsell.upsell_products.extend(selection)
                else:
                    upsell.upsell_products  = selection
                upsell.save()
            
            # the stats are rudementary now and are at the highest level (shop)
            # however, let's create it here, since we wil be attaching the stats to the main product anways
            upsell_stats = UpsellStats.objects.filter(shop = shop).first()
            
            #TODO: this should actually be created on install...not here
            if not upsell_stats:
                upsell_stats              = UpsellStats()
                upsell_stats.shop         = shop
                upsell_stats.views        = 0
                upsell_stats.clicks       = 0
                upsell_stats.sales        = 0
                upsell_stats.sales_amount = 0
                upsell_stats.save()
            
            #TODO: the response returned here should be do a post to the dashboard to remove
            # the products selected
            response = HttpResponse()
            
            return response
        except Exception as e:
            logger.error(e)
            return HttpResponse()


@csrf_exempt
def removeSelectedUpsell(request):
    shop_name = request.session['shop']
    if request.method == 'POST':
        logger.debug("sending post data via /saveSelectedUpsell")
        try:
            json_data   = json.loads(request.body)
            product_id  = json_data.get('product_id') 
            selection   = json_data.get('selection')
            
            # store the product and its upsells in the db
            
            upsell = Upsell.objects.filter(main_product = product_id).first()
            shop   = Shop.objects.filter(domain = shop_name).first()
            
            if not upsell:
                logger.error("Tried to remove upsell from a product that doesn't exist in DB. Something is wrong")
            else:
                if upsell.upsell_products and upsell.upsell_products != '':
                    for item in selection:
                        if item in upsell.upsell_products:
                            upsell.upsell_products.remove(item)
                            upsell.save()
                else:
                    logger.error("There are no upsell products to remove from this product")
            
            #TODO: the response returned here should be do a post to the dashboard to remove
            # the products selected
            response = HttpResponse()
            
            return response
        except Exception as e:
            logger.error(e)
            return HttpResponse()

@csrf_exempt
def chosenUpsellCheckout(request):
    # here we'll get the product ids of the upsell, quantity and variant chosen for that product
    # we then create the link that will take the user to the checkout page
    # in between this we need to create a cookie to track sales referral
    if request.method == 'OPTIONS':
        return _init_callback_response_header(request)
    
    
    
    try:
        # here we're getting the multiline JSON containing the upsell products the user chose
        json_data       = json.loads(request.body)
        logger.info(json_data)
        permalink = []
        
        logger.info("json data" + str(json_data))
        
        # we create the shop permalink direct to checkout link here first, we append it with the variant
        # and qanitty info later on
        shop_domain     = request.session['shop_domain']
        logger.info(shop_domain)
        shop = Shop.objects.filter(domain = shop_domain).first()
        shopify_permlink_url = 'https://'+str(shop_domain)+'/cart/'
        
        if not shop:
            logger.error("we couldn't find the shop uh oh")
        
        # update clicks stats
        upsell_stats = UpsellStats.objects.filter(shop = shop).first()
        if not upsell_stats:
            logger.error("somethign is wrong, not upsell_stats row for this product")
        else:
            upsell_stats.clicks += 1
            upsell_stats.save()
        
        # we go throuh each line in the JSON and extract the varaint ID from it
        for line in json_data:
            product_id      = line.get('product_id')
            ref             = line.get('ref') 
            quantity        = line.get('quantity')
            variant0        = line.get('variant0','')
            variant0_value  = line.get('variant0_value','')
            variant1        = line.get('variant1','')
            variant1_value  = line.get('variant1_value','')
            variant2        = line.get('variant2','')
            variant2_value  = line.get('variant2_value','')
            

            # we do a Shopify API call in order to find the variant id for every upsell product
            session  = shopify.Session(shop.domain, shop.access_token)
            shopify.ShopifyResource.activate_session(session)
            product         = shopify.Product.find(int(product_id))   
            
            # if there's no variant for the upsell product, then we can just get the variant id quickly
            if variant0 == '':
                #get variant id of position 0
                try:
                    variant_id = product.variants[0].id
                    logger.info("the quantity is " + str(quantity))
                    shopify_permlink_url = shopify_permlink_url + str(variant_id) + ":" + str(quantity) + ','
                except Exception as e:
                    logger.error(e)
                    logger.error("productid not found in shop")
                    return HttpResponse()
            
            # if there is a variant, we need to find it in the Shopify Product object
            # we need to match the the variant values we have in the JSON with the one in the variant for 
            # that particular product ID. We distinguish the amount of variants here as well
            else:
                variants      = product.variants
                final_variant = ''

                logger.info("product options length" + str(len(product.options)))
                variant_found = False
                for variant in variants:
                    if len(product.options) == 1:
                        if variant.option1 == variant0_value:
                            variant_found = True
                            final_variant = variant.id
                        
                    elif len(product.options) == 2:
                        if variant.option1 == variant0_value and variant.option2 == variant1_value:
                            variant_found = True
                            final_variant = variant.id
                                           
                    elif len(product.options) == 3:
                        if variant.option1 == variant0_value and variant.option2 == variant1_value and variant.option3 == variant2_value:
                            variant_found = True
                            final_variant = variant.id
                    
                
                if not variant_found:
                    logger.error("The variant was not found and therefore the checkout permalink will not work")
                    return HttpResponse()
                permalink.append(quantity)
                shopify_permlink_url = shopify_permlink_url + str(final_variant) + ":" + str(quantity) +','
    except Exception as e:
        logger.error(e)
        return HttpResponse()

    
    # remove last comma charactar from string
    shopify_permlink_url = shopify_permlink_url[:-1]
    shopify_permlink_url = shopify_permlink_url + '/?ref=' + str(ref)
    logger.info(shopify_permlink_url)
        
    response = HttpResponse("", content_type='application/json')
    response.write(json.dumps({'permalink': shopify_permlink_url}))

    
    origin = request.META.get('HTTP_ORIGIN')
    response['Access-Control-Allow-Origin'] = origin
    response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
    response['Access-Control-Allow-Credentials'] = 'true'
    response['Access-Control-Allow-Headers'] = request.META.get('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', "X-Requested-With, Content-Type, Content-Length, Origin, Accept, Accept-Encoding, Referer, User-Agent, Host, Cookie")
    
    return _init_callback_response_header(request, response)



def generate_ref(prefix = "UP"):
    return "{0}{1}".format(prefix, base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', ''))
    