from django.db import models
import ast

class ListField(models.TextField):
    __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return unicode(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)

# Create your models here.
# Create your models here.
class Shop(models.Model):
    domain                    = models.CharField(max_length=255, unique=True, db_column='shop_url')
    name                      = models.CharField(max_length=255, db_column='shop_name')
    shop_email                = models.CharField(max_length=255)
    access_token              = models.CharField(max_length=255)
    login_cookie              = models.CharField(max_length=255)
    currency                  = models.CharField(max_length=255, default='USD')
    is_installed              = models.BooleanField(default=True)
    created_on                = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_on                = models.DateTimeField(auto_now=True, auto_now_add=True)
    recurring_charge_id       = models.IntegerField(null=True, blank=True)


class Upsell(models.Model):
    shop                      = models.ForeignKey('Shop')
    main_product              = models.IntegerField(null=True, blank=True)
    upsell_products           = ListField()
    created_on                = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_on                = models.DateTimeField(auto_now=True, auto_now_add=True)
    
class UpsellStats(models.Model):
    shop                      = models.ForeignKey('Shop')
    views                     = models.IntegerField(null=True, blank=True, default = 0)
    clicks                    = models.IntegerField(null=True, blank=True, default = 0)
    sales                     = models.IntegerField(null=True, blank=True, default = 0)
    sales_amount              = models.FloatField(null=True, blank=True, default = 0)
    
    
class ReferralRecord(models.Model):
    shop                        = models.ForeignKey('Shop')
    created_on                  = models.DateTimeField(auto_now=False, auto_now_add=True)
    ref                         = models.CharField(max_length=255)
    order_id                    = models.BigIntegerField(default = 0)
    customer_id                 = models.BigIntegerField(default = 0)
    customer_first_name         = models.CharField(max_length=255)
    customer_last_name          = models.CharField(max_length=255)
    customer_email              = models.CharField(max_length=255)